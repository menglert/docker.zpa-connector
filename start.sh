#!/bin/bash

if [ -e "${ZSCALER_ROOT}/var/cert.pem" ]
then
    echo "Already enrolled."
elif [ -n "${ZPA_PROV_KEY:+1}" ]
then
    echo ${ZPA_PROV_KEY} >> ${ZSCALER_ROOT}/var/provision_key
else
    echo "No provisioning key provided. I'm not starting."
    exit 1
fi

{ sleep 20; journalctl -u zpa-connector -f; } &

exec /usr/sbin/init