FROM centos/systemd
LABEL maintainer="Michael Englert <michi.e@tuta.io>"

ENV ZSCALER_ROOT /opt/zscaler

ADD zscaler.repo /etc/yum.repos.d/
ADD start.sh /

RUN yum update -y \
    && yum install -y zpa-connector \
    && yum clean all \
    && systemctl enable zpa-connector

CMD ["/start.sh"]